#!/bin/bash

set -o errexit
set -o nounset
#set -o xtrace
set -o pipefail

WORKING_DIR="$(mktemp -dt simple_ci_gateway_iso_builder.XXXXXXXXXX)"
BUILD_DATE=$(date +'%Y%m%d')
BUILD_TIME=$(date +'%H%M%S')
OUTPUT_DIR=$(pwd)/output-${BUILD_DATE}-${BUILD_TIME}
PGP_KEYSERVER=${PGP_KEYSERVER:-hkps://keys.openpgp.org/}
PGP_FINGERPRINT=${PGP_FINGERPRINT:-58A95EC95DCD89ECC0D4DD2AE1773D4D6C35A544}

sudo -p "Enter root password for ISO generation stages: " whoami

build_dnsmasq() {
    local old_workdir
    local workdir
    local root
    old_workdir="$(pwd)"
    workdir="$OUTPUT_DIR"/dnsmasq
    root="$(pwd)"/releng/airootfs

    already_built=
    for pkg in "$workdir"/dnsmasq-nftset-git-*.pkg.tar.zst; do
	if [ -f "$pkg" ]; then
	    already_built=1
	    break
	fi
    done
    if [ -z "$already_built" ]; then
	# remove any old version in $root
	rm -f "$root"/dnsmasq-*.pkg.tar.zst

        echo "dnsmasq-git needs to be built..."
        [ -d "$workdir" ] && rmdir "$workdir"

        git clone https://aur.archlinux.org/dnsmasq-git.git "$workdir"
        cd "$workdir"
        # TODO: workaround until dnsmasq-git is built with this option enabled
        # https://aur.archlinux.org/packages/dnsmasq-git/#comment-828780
        # https://bugs.archlinux.org/task/72286?project=1&string=dnsmasq
        sed -i 's|_copts="-DHAVE_DBUS|_copts="-DHAVE_NFTSET -DHAVE_DBUS|' PKGBUILD

        makepkg -rs

        cd "$old_workdir"
    fi

    cp "$workdir"/dnsmasq-git-*.pkg.tar.zst "$root"/
}

iso() {
    ROOT="$(pwd)"/releng/airootfs

    FARM_NAME=${FARM_NAME?Farm name not supplied}
    GITLAB_REGISTRATION_TOKEN=${GITLAB_REGISTRATION_TOKEN?Gitlab registration token must be suppled}
    GITLAB_ACCESS_TOKEN=${GITLAB_ACCESS_TOKEN?Gitlab access token must be suppled}

    rm -rf "$ROOT"/etc/simple_ci_gateway/
    mkdir -v "$ROOT"/etc/simple_ci_gateway/
    cat <<EOF >"$ROOT"/etc/simple_ci_gateway/private.env
FARM_NAME=$FARM_NAME
GITLAB_REGISTRATION_TOKEN=$GITLAB_REGISTRATION_TOKEN
GITLAB_ACCESS_TOKEN=$GITLAB_ACCESS_TOKEN
EOF

    rm -rf "$ROOT"/root/.ssh/
    rm -rf "$OUTPUT_DIR"
    mkdir -pv "$OUTPUT_DIR"

    mkdir -pv "$ROOT"/root/
    cat <<EOF >"$ROOT"/root/.zlogin
url="https://druid.fish/public/installer-${BUILD_DATE}.asc"

gpg --batch \
    --keyserver ${PGP_KEYSERVER} \
    --recv-keys ${PGP_FINGERPRINT}  2>&1 | tee installer.log

if [ $? -eq 0 ] && curl --output /dev/null --silent --head --fail "\$url"; then
    curl -o /usr/local/bin/installer.asc "\$url"
    if ! gpg --batch --yes --output /usr/local/bin/installer /usr/local/bin/installer.asc; then
	echo "Failed to validate the signature of the installer." | tee installer.log
    else
        /usr/local/bin/installer 2>&1 | tee installer.log
    fi
else
    echo "No remote installer available, using the installer in the base ISO image..." | tee installer.log
    /usr/local/bin/installer 2>&1 | tee installer.log
fi
EOF

    mkdir "$ROOT"/root/.ssh/

    keyname=${OUTPUT_DIR}/${FARM_NAME}-ssh_key-$(date +%Y.%m.%d)
    ssh-keygen -C "simple ci gateway admin" -f "$keyname" -P ""
    cp "${keyname}".pub "$ROOT"/root/.ssh/authorized_keys

    build_dnsmasq

    sudo mkarchiso -v -w "$WORKING_DIR/iso" -o "$WORKING_DIR/build" releng
    cp -v "$WORKING_DIR"/build/simple_ci_gateway-*-x86_64.iso "${OUTPUT_DIR}/${FARM_NAME}-$(date +%Y.%m.%d)"-x86_64-installer.iso
    ( cd "$OUTPUT_DIR" && sha256sum -b ./*.iso > SHA256SUMS.txt )

    sudo rm -rf "$WORKING_DIR"
    echo "ISO generated into ${OUTPUT_DIR}"
}

iso

# vim: ts=4 sw=4 et
