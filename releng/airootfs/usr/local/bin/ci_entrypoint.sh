#!/bin/bash
# -*- mode: shell-script -*-

set -o nounset
set -o pipefail
set -o xtrace

# Allow the client to work across the VPN
#ip route add 192.168.10.0/24 via 192.168.42.149

cd "$HOME" || exit
[ -d valve-infra ] || git clone https://gitlab.freedesktop.org/mupuf/valve-infra.git

docker pull registry.freedesktop.org/mupuf/valve-infra/valve-infra:latest || exit

cd valve-infra || exit
git fetch -a origin

if /bin/true; then
        git reset --hard origin/master
        params="--pull=always"
else
        # Debug env
        git reset --hard origin/infra-mupuf || exit 1
        params="--pull=never --env VALVE_INFRA_NO_PULL=1"
        ./local_build.sh
fi

docker run -i --rm --name valve-infra \
  "$params" \
  --privileged \
  --network=host \
  `# Setup by the Simple CI Gateway environment` \
  --env-file /etc/simple_ci_gateway/private.env \
  -v "$(pwd)":/app \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /mnt:/mnt \
  registry.freedesktop.org/mupuf/valve-infra/valve-infra:latest
