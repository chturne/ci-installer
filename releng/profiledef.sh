#!/usr/bin/env bash
# shellcheck disable=SC2034

iso_name="simple_ci_gateway"
iso_label="SIMPLE_CI_GATEWAY_$(date +%Y%m)"
iso_publisher="Simple CI Gateway <https://gitlab.freedesktop.org/mupuf/simple-ci-gateway>"
iso_application="Simple CI Gateway Installer"
iso_version="$(date +%Y.%m.%d)"
install_dir="arch"
bootmodes=('bios.syslinux.mbr' 'bios.syslinux.eltorito' 'uefi-x64.systemd-boot.esp' 'uefi-x64.systemd-boot.eltorito')
arch="x86_64"
pacman_conf="pacman.conf"
airootfs_image_type="squashfs"
airootfs_image_tool_options=('-comp' 'xz' '-Xbcj' 'x86' '-b' '1M' '-Xdict-size' '1M')
file_permissions=(
  ["/etc/shadow"]="0:0:400"
  ["/root"]="0:0:750"
  ["/root/.ssh"]="0:0:0700"
  ["/root/.ssh/authorized_keys"]="0:0:0600"
  ["/etc/simple_ci_gateway"]="0:0:750"
  ["/etc/simple_ci_gateway/private.env"]="0:0:400"
  ["/usr/local/bin/choose-mirror"]="0:0:755"
  ["/usr/local/bin/ci_entrypoint.sh"]="0:0:755"
  ["/usr/local/bin/installer"]="0:0:755"
  ["/usr/local/bin/selftest"]="0:0:755"
)

