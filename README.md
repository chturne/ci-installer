# Build an installer image

Construct the ISO file for a specific site,

    env FARM_NAME=tchar-virt \
        GITLAB_REGISTRATION_TOKEN=$(pass valve-infra/virt/gitlab_registration_token) \
	GITLAB_ACCESS_TOKEN=$(pass valve-infra/virt/gitlab_access_token) \
	./build.sh iso

This will produce an ISO in the `./output-YYMMDD-HHMMSS` directory
with a name like `<farm_name>-$(date +%Y.%m.%d)-x86_64.iso`.

# Test the installer image

WARNING: This section is out of date, the virtual testing component
now lives in =valve-infra/vivian=.

Test the ISO installer created above using QEMU, this will create a
disk file which can be validated later.

	./build.sh test_installer ./output/<farm_name>-$(date +%Y.%m.%d)-x86_64.iso

Note: if you get an `Operation not permitted` error from qemu when it tries to
create/configure the tap, you may have to give `CAP_NET_ADMIN` capabilities to
qemu, for example:

    sudo setcap cap_net_admin+ep $(which qemu-system-x86_64)

After installation, test the resulting gateway disk iamge,

	./build.sh test_gateway

By default, the installer and disk will boot in legacy BIOS mode. You
can make this explicit with `./build.sh --bios ...`, or use a UEFI
flash with `./build.sh --uefi ...`

You can connect to either the live CD or the installed system thusly,

    ssh -i ./output/gateway_ssh_key root@localhost -p 60022 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null

